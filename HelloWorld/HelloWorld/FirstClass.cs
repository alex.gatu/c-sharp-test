﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld
{
    class FirstClass
    {
        static void Main(string[] args)
        {
            Console.WriteLine("hello world !");
            Test();

            Console.ReadKey();

            if (args.Length == 3)
            {
                float a = float.Parse(args[0]);
                float b = float.Parse(args[2]);
                int x = int.Parse(args[0]);
                string op = args[1];
                // implement calculator logic here
                //Console.WriteLine("Result is: " + result);
            }
            else
            {
                Console.WriteLine("3 args are needed!");
            }

        }

        public void IfExample(int x)
        {
            if (x >= 0)
            {
                Console.WriteLine(" Number is positive !");
            }
            else
            {
                Console.WriteLine(" Number is negative !");
            }
        }

        public void SwitchExample(int switchVar)
        {
            switch(switchVar)
            {
                case 1:
                    {
                        Console.WriteLine("Monday");
                        break;
                    }
                case 7:
                    {
                        Console.WriteLine("Sunday");
                        break;
                    }
                default:
                    {
                        Console.WriteLine("Other");
                        break;
                    }

            }
        }

        public void WhileExample(int max)
        {
            int counter = 0;
            while(counter <= max)
            {
                Console.WriteLine("Counter: " + counter.ToString());
                counter++;
            }
        }

        public void DoWhileExample(int max)
        {
            int counter = 0;
            do
            {
                Console.WriteLine("Counter: " + counter.ToString());
                counter++;
            }
            while (counter <= max);
        }

        public static void Test()
        {
            int[] numbers = { 10, 20, 30, 40, 50 };
			//LINQ
            var y = numbers.OrderBy(s => s);
            int sum = 0;
            foreach (int x in numbers)
            {
                if (x == 30)
                {
                    continue;
                }
                sum += x;
                if (sum > 100)
                {
                    break;
                }
                Console.WriteLine(x);                
            }
            Console.WriteLine("sum: " + sum);
            Console.WriteLine(typeof(int));
            DateTime value = new DateTime(2018, 3, 6);
            Console.WriteLine(value);

        }

        public static int Test(int testVal, int target)
        {
            if (testVal > target)
                return +1;
            else if (testVal < target)
                return -1;
            else
                return 0;
        }

        public static int Test2(int testVal, int target)
        {
            int result = 0;
            if (testVal > target)
                result = +1;
            else if (testVal < target)
                result = -1;
            else
                result = 0;
            return result;
        }


    }
}
