﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    public sealed class Square : Rectangle
    {
        double l;

        public override void Draw()
        {
            Console.WriteLine("This is the Draw() method from Square class");
        }

        public override double GetArea()
        {
            return base.GetArea(l,l);
        }
    }
}
