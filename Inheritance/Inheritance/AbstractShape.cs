﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    public abstract class AbstractShape : IShape
    {
        public abstract void ComputeLength();

        public virtual double GetArea()
        {
            return 10;
        }
    }
}
