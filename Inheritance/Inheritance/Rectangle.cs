﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    public class Rectangle : Shape
    {
        double width;
        double height;

        public Rectangle()
        {
            width = 0;
            height = 0;
        }

        public Rectangle (double w, double h)
        {
            this.width = w;
            this.height = h;
        }

        public virtual double GetArea()
        {
            return width * height;
        }


        public virtual double GetArea(double w, double h)
        {
            return w * h;
        }

        protected override void Erase()
        {

        }

        public override void Draw()
        {
            Console.WriteLine("This is the Draw() method from Rectangle class");
        }

        public void MyMethod()
        {
            Erase();
            
        }
    }
}
