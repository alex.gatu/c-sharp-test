﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    class Circle : Shape
    {
        public override void Draw()
        {
            Console.WriteLine("This is the Draw() method from Circle class");
        }
    }
}
