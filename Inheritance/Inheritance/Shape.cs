﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    public class Shape : AbstractShape
    {
        string name;
        public string Name
        {
            get
            {
                return "The name of the shape is " + name;
            }
            set
            {
                name = value;
            }
        }

        public override void ComputeLength()
        {
            
        }

        public override double GetArea()
        {
            return 0;
        }

        public override string ToString()
        {
            return "Custom Shape method ToString: " + base.ToString();
        }

        public virtual void Draw(Shape s)
        {
            ComputeLength();
            Console.WriteLine("shape s draw custom");
        }

        public virtual void Draw()
        {
            Console.WriteLine("This is the Draw() method from Shape class");
        }

        protected virtual void Erase()
        {
            Console.WriteLine("This is the Erase() method from Shape class");
        }

        protected void GetColor()
        {
            Console.WriteLine("This is the GetColor() method from Shape class");
        }

        protected void SetColor()
        {
            Console.WriteLine("This is the SetColor() method from Shape class");
        }

        // Private methods are not inherited.
        private void MyPrivateMethod()
        {            

        }

        public class NestedShapeClass
        {

        }
    }
}
