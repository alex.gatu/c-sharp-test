﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    class Program : Rectangle
    {
        static void InheritanceExamples()
        {
            Shape s1 = new Shape();
            Rectangle r1 = new Rectangle();
            Triangle t1 = new Triangle();
            Square sq1 = new Square();
            s1.Draw();
            r1.Draw();
            sq1.Draw();
            //sq1.GetArea();
            //r1.GetArea();

            Console.WriteLine(s1.ToString());

            // You can instantiate a base class type with a child class constructor
            // this basically converts the Shape to a Rectangle or Circle
            Shape r2 = new Rectangle();
            r2.Draw();
            Shape c2 = new Circle();
            c2.Draw();

            // You cannot create an instance of the base class in the form of a child class
            //Rectangle r3 = new Shape();  


            // s1 is a Shape and the Draw requires a Shape as parameter, but we put a Square
            // this is because of ploymorphism
            s1.Draw(sq1);

            // you cannot instantiate an interface or an abstract class
            // IShape isp = new IShape();
            //AbstractShape asp = new AbstractShape();
        }

        static void CollectionsExamples()
        {
            int[] myArray = new int[5] { 1, 2, 3, 4, 5 };


            Object[] myObjArray = new Object[5] { 1, 3, 5, 7, 9 };

            foreach (int val in myObjArray)
            {
                Console.WriteLine(val);
            }
            Console.WriteLine("Length of the array is " + myObjArray.Length);
            for (int i = 0; i < myObjArray.Length; i++)
            {
                Console.WriteLine(myObjArray[i]);
            }

            Shape s1 = new Shape();
            Circle c1 = new Circle();

            Shape[] myShapeArray = new Shape[2] { s1, c1 };

            List<string> myList = new List<string>();
            myList.Add("Romania");
            myList.Add("Rep. MD");
            myList.Add("Bulgaria");
            myList.Add("Bulgaria");
            foreach (string val in myList)
            {
                Console.WriteLine("At index " + myList.IndexOf(val) + " we have " + val);
            }

            myList.RemoveAt(0);

            for (int i = 0; i < myList.Count; i++)
            {
                Console.WriteLine("At index " + i + " we have " + myList.ElementAt(i));
            }
            Dictionary<string, string> codes = new Dictionary<string, string>();
            codes.Add("RO", "Romania");
            codes.Add("MD", "Rep. Moldova");

            Console.WriteLine(codes["RO"]);


            if (codes.Keys.Contains("RO"))
            {
                Console.WriteLine("Key already present");
            }
            else
            {
                codes.Add("RO", "ROmania");
            }

            foreach (KeyValuePair<string, string> kvp in codes)
            {
                Console.WriteLine("[" + kvp.Key + "] : " + kvp.Value);
            }

            List<string> keys = codes.Keys.ToList();
            foreach (string key in keys)
            {
                Console.WriteLine(key);
            }

            foreach (string key in codes.Keys)
            {
                Console.WriteLine("[" + key + "] : " + codes[key]);
            }
        }

        public enum Browsers
        {
            Chrome,
            Firefox,
            InternetExplorer
        }



        static void Main(string[] args)
        {
            Dictionary<string, List<string>> countries = new Dictionary<string, List<string>>();

            List<string> countriesInEurope = new List<string>();
            countriesInEurope.Add("Romania");
            countriesInEurope.Add("Rep. MD");
            countriesInEurope.Add("Bulgaria");

            List<string> countriesInAsia = new List<string>
            {
                "North Korea",
                "South Korea"
            };

            countries.Add("Europe", countriesInEurope);
            countries.Add("Asia", countriesInAsia);

            //foreach (KeyValuePair<string, List<string>> kvp in countries)
            //{
            //    Console.WriteLine("The following countries are in continent: " + kvp.Key);
            //    foreach (string country in kvp.Value)
            //    {
            //        Console.WriteLine(" --> " + country);
            //    }
            //}
            SortedList<int, string> mySortedList = new SortedList<int, string>();
            mySortedList.Add(6, "string 1");
            mySortedList.Add(2, "string 2");
            mySortedList.Add(300, "string 3");
            mySortedList.Add(61, "string 4");
            mySortedList.Add(1, "string 5");

            //foreach(KeyValuePair<int,string> kvp in mySortedList)
            //{
            //    Console.WriteLine("Key: " + kvp.Key + " -- Value: " + kvp.Value);
                  //Console.WriteLine("Key:{0} -- Value:{1} ", kvp.Key, kvp.Value);
            //}

            //int[] myArray = new int[6] { 1,2,5,1,5,1 };
            //HashSet<int> hs = new HashSet<int>(myArray);
            //hs.Remove(1);
            //foreach (int val in hs)
            //{
            //    Console.WriteLine(val);
            //}
            Queue<string> myQueue = new Queue<string>();
            myQueue.Enqueue("Automation");
            myQueue.Enqueue("Testing");
            myQueue.Enqueue("rulz!");
            foreach (string val in myQueue)
            {
                Console.WriteLine(" {0} ", val);
            }
            Stack<string> myStack = new Stack<string>();
            myStack.Push("Automation");
            myStack.Push("Testing");
            myStack.Push("rulz!");

            foreach (string val in myStack)
            {
                Console.WriteLine(" {0} ", val);
            }

            Browsers br = Browsers.Firefox;
            int b = (int)Browsers.Firefox;
            Console.WriteLine("{0} - {1}",b, br );
            foreach (Browsers brw in Enum.GetValues(typeof(Browsers)))
            {
                Console.WriteLine(brw);
            }

            Shape s = new Shape();
            s.Name = "ceva";
            Console.WriteLine(s.Name);

            Console.ReadKey();
        }
    }
}
