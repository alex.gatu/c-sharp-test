﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    class Triangle : Shape
    {
        public void FlipVertical()
        {
            Console.WriteLine("This is the FlipVertical() method from Shape class");
        }

        public void FlipHorizontal()
        {
            Console.WriteLine("This is the FlipHorizontal() method from Shape class");
        }

    }
}
